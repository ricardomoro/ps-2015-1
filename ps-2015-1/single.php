<?php get_header(); ?>

<div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <article class="col-xs-12 col-md-8">
            <h2><?php the_title(); ?></h2>
            <small class="pull-right">publicado em <?php the_time('d'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></small>
            <br/>
            <?php if ( has_post_thumbnail()): ?>
                <div class="pull-left">
                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <a href="<?php echo $url; ?>" rel="prettyPhoto">
                        <?php the_post_thumbnail( 'medium', array('class' => 'img-reponsive img-thumbnail') ); ?>
                    </a>
                </div>
            <?php endif; ?>
            <?php the_content(); ?>
        </article>
    <?php endwhile; ?>

    <aside class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </aside>
</div>

<?php get_footer(); ?>
