<div class="row">
    <div class="col-xs-12">
        <?php if (!dynamic_sidebar('banner')) : ?>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <?php
        $menu_slug = 'top-menu';
        $locations = get_nav_menu_locations();

        if (isset($locations['menu-lateral'])) {
            $menu_id = $locations['menu-lateral'];
        }
        $items = wp_get_nav_menu_items( $menu_id );

        foreach ($items as $key => $item) :
            if ($key % 4 == 0) {
                $class = "alt4";
            } else if ($key % 3 == 0) {
                $class = "alt3";
            } else if ($key % 2 == 0) {
                $class = "alt2";
            } else {
                $class = "alt1";
            }
    ?>
        <div class="col-xs-6">
            <a href="<?php echo $item->url; ?>" target="<?php echo $item->target; ?>" class="atalho <?php echo $class; ?>"><?php echo $item->title; ?></a>
        </div>
    <?php endforeach; ?>
    <div class="col-xs-6">
        <a href="<?php echo get_permalink( get_page_by_path( 'datas-importantes' ) ); ?>" class="atalho" id="datas">
            <span class="glyphicon glyphicon-calendar"></span><br/>
            Datas importantes
        </a>
    </div>
    <div class="col-xs-6">
        <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>" class="atalho" id="editais">
            <span class="glyphicon glyphicon-file"></span><br/>
            Editais
        </a>
    </div>
</div>
