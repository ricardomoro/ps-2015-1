<?php get_header(); ?>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="row">
            <div class="col-xs-12">
                <h2>Todos os Avisos</h2>
            </div>
        </div>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row">
            <div class="col-xs-12">
                <article>
                    <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                    <p><?php the_excerpt(); ?></p>
                    <small><?php the_time('d'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></small>
                    <a href="<?php the_permalink() ?>" rel="bookmark" class="btn btn-success pull-right">
                        Leia mais<span class="sr-only"> sobre &ldquo;<?php the_title(); ?>&rdquo;</span>
                    </a>
                    <hr />
                </article>
            </div>
        </div>
        <?php endwhile; endif; ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
