<?php get_header(); ?>

<section class="row">
    <div class="col-xs-12 col-md-8">
        <h2>Editais</h2>
        <?php echo get_template_part('partials/loop', 'editais'); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="row">
            <div class="col-xs-12">
                <h3>Tipos de Edital</h3>
                <div class="list-group">
                <?php $tipos_terms = get_terms( 'tipo' ); ?>
                <?php foreach ($tipos_terms as $key => $tipo) : ?>
                    <a href="<?php echo get_term_link($tipo); ?>" class="list-group-item">
                        <?php echo $tipo->name; ?>
                    </a>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
    </div>
</section>

<?php get_footer(); ?>
