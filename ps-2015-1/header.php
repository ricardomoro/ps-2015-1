<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Comunicação do IFRS">

    <!-- Título -->
    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title( '-', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        // $site_description = get_bloginfo( 'description', 'display' );
        // if ( $site_description && ( is_home() || is_front_page() ) )
        //     echo ' - '.$site_description;

        // Add a page number if necessary:
        if ( $paged >= 2 || $page >= 2 )
            echo ' - ' . sprintf( 'Página %s', max( $paged, $page ) );
    ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico"/>

    <!-- CSS & JS -->
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
    <link rel="alternate stylesheet" type="text/css" title="contraste" href="<?php echo get_stylesheet_directory_uri(); ?>/style-contraste.css" />

    <?php echo get_template_part('partials/ltie9'); ?>
</head>
<body>
    <a href="#inicio-conteudo" accesskey="1" class="sr-only">Conteúdo <span class="badge">1</span></a>

    <?php echo get_template_part('partials/barrabrasil'); ?>
                    
    <header>
        <div class="container">
            <!-- Barra de Acessibilidade -->
            <div class="row">
                <div class="col-xs-12">
                    <ul class="sr-only">
                        <li><a href="#inicio-menu" accesskey="2">Menu <span class="badge">2</span></a></li>
                        <!-- <li><a href="#search-acessibilidade" accesskey="3">Pesquisar <span class="badge">3</span></a></li> -->
                    </ul>
                    <!-- <ul class="pull-right">
                        <li><a href="#contraste-switch"           accesskey="4" id="contraste-switch"><span class="glyphicon glyphicon-adjust"></span> <span class="sr-only">Alterar </span>Contraste <span class="badge">4</span></a></li>
                    </ul> -->
                </div>
            </div>
            <!-- Topo -->
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
                    <div id="logo-ifrs">
                        <h1>
                            <a href="<?php bloginfo('url'); ?>" title="Página Inicial">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-ifrs.png" alt="Logo do IFRS" />
                            </a>
                        </h1>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4 hidden-xs">
                    <div id="foto-alunos">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/foto-ingresso.jpg" />
                    </div>
                </div>
                <div class="col-md-3 col-lg-4 hidden-xs hidden-sm">
                    <div id="ensino-publico">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ensino-gratuito.png" class="img-responsive"/>
                    </div>
                    <div id="total-cursos" class="visible-md">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/total-cursos.png" class="img-responsive" />
                    </div>
                </div>
            </div>
        </div>
        <div class="menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-5 col-lg-4">
                        <a href="#inicio-menu" id="inicio-menu" class="sr-only sr-only-focusable">In&iacute;cio do menu</a>
                        <nav class="navbar navbar-default navbar-ingresso" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-principal">
                                    <span class="sr-only">Alternar navega&ccedil;&atilde;o</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="menu-principal">
                                <?php
                                    wp_nav_menu( array(
                                        'menu'              => 'menu-principal',
                                        'theme_location'    => 'menu-principal',
                                        'depth'             => 2,
                                        'container'         => false,
                                        'container_class'   => 'collapse navbar-collapse',
                                        'container_id'      => 'menu-principal',
                                        'menu_class'        => 'nav navbar-nav',
                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                        'walker'            => new wp_bootstrap_navwalker())
                                    );
                                ?>
                            </div>
                        </nav>
                        <a href="#fim-menu" id="fim-menu" class="sr-only sr-only-focusable">Fim do menu</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-5 col-lg-4">
                    <div id="inscricoes">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/data-prova.png" class="img-responsive" />
                    </div> 
                </div>
                <div class="col-xs-6 col-md-offset-4 col-md-3 col-lg-offset-4 col-lg-4 hidden-sm hidden-md">
                    <div id="total-cursos">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/total-cursos.png" class="img-responsive" />
                    </div>
                </div>
            </div>
        </div>
    </header>

    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only sr-only-focusable">In&iacute;cio do conte&uacute;do</a>

    <?php breadcrumb(); ?>
    
    <section class="container" id="conteudo">
