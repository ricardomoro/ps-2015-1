<?php
    // CSS
    wp_enqueue_style( 'css-dataTables', get_stylesheet_directory_uri() . '/css/jquery.dataTables.min.css' );
    wp_enqueue_style( 'css-dataTables-bootstrap', get_stylesheet_directory_uri() . '/css/dataTables.bootstrap.css' );

    // JS
    wp_enqueue_script( 'js-dataTables', get_stylesheet_directory_uri() . '/js/jquery.dataTables.min.js', array(), false, true );
    wp_enqueue_script( 'js-dataTables-bootstrap', get_stylesheet_directory_uri() . '/js/dataTables.bootstrap.js', array(), false, true );
    wp_enqueue_script( 'js-cursos-dataTables', get_stylesheet_directory_uri() . '/js/cursos-dataTables.js', array(), false, true );
?>
<?php get_header(); ?>

<section class="row">
    <div class="col-xs-12 col-md-8">
        <h2> Cursos na modalidade de ensino <?php single_term_title(); ?></h2>
        <!-- Nav tabs -->
        <ul class="nav nav-pills nav-campus" role="tablist">
            <?php $terms = get_terms('campus'); ?>
            <?php foreach ($terms as $key => $campus) : ?>
                <li <?php echo ($key == 0) ? 'class="active"' : ''; ?>><a href="#tab<?php echo $campus->term_id; ?>" role="tab" data-toggle="tab"><?php echo $campus->name; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($terms as $key => $campus) : ?>
            <div class="tab-pane fade<?php echo ($key == 0) ? ' in active' : ''; ?>" id="tab<?php echo $campus->term_id; ?>">
                <div class="table-responsive">
                    <table class="table table-striped table-cursos">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>C&acirc;mpus</th>
                                <th>Turnos</th>
                                <th>Vagas *</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                global $wp_query;
                                $args = array(
                                    'post_type' => 'curso',
                                    'orderby' => 'title',
                                    'order' => 'ASC',
                                    'campus' => $campus->slug,
                                );
                                $args = array_merge($wp_query->query_vars, $args);
                                query_posts($args);
                            ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <tr>
                                    <td><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'campus') as $campus) : ?>
                                            <!-- <p><a href="<?php echo get_term_link($campus); ?>"><?php echo $campus->name; ?></a></p> -->
                                            <p><?php echo $campus->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach (get_the_terms(get_the_ID(), 'turno') as $turno) : ?>
                                            <p><?php echo $turno->name; ?></p>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <p><?php echo get_post_meta(get_the_ID(), 'vagas', true); ?></p>
                                    </td>
                                </tr>
                            <?php endwhile;?>
                            <?php wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-warning" role="alert">
                    <p><strong>*</strong> Para ver a forma de distribui&ccedil;&atilde;o das vagas, leia os <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>">editais</a>.</p>
                </div>
            </div>
        </div>
        <a href="<?php echo get_post_type_archive_link( 'curso' ); ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Voltar para a lista de cursos</a>
    </div>
    <div class="col-xs-12 col-md-4">
        <aside>
            <?php echo get_template_part('partials/atalhos', 'home'); ?>
        </aside>
    </div>
</section>

<?php get_footer(); ?>
