<?php get_header(); ?>

<div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <article class="col-xs-12 col-md-8">
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </article>
    <?php endwhile; ?>

    <aside class="col-xs-12 col-md-4">
        <?php if (count(get_pages(array('child_of' => get_the_ID()))) > 0) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-stacked">
                    <?php
                        wp_list_pages( array(
                            'child_of' => get_the_ID(),
                            'title_li' => '',
                            'depth' => '1',
                        ) );
                    ?>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </aside>
</div>

<?php get_footer(); ?>
