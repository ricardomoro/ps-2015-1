<?php get_header(); ?>

<div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <article class="col-xs-12 col-md-8">
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="panel panel-default panel-curso">
                        <div class="panel-heading">
                            <h3>Vagas</h3>
                        </div>
                        <div class="panel-body">
                            <?php echo get_post_meta(get_the_ID(), 'vagas', true); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="panel panel-default panel-curso">
                        <div class="panel-heading">
                            <h3>Curso oferecido nos Câmpus</h3>
                        </div>
                        <div class="panel-body">
                        <?php
                            $campus = get_the_terms(get_the_ID(), 'campus');
                            foreach ($campus as $key => $value) :
                        ?>
                                <p><a href="<?php echo get_term_link($value); ?>"><?php echo $value->name; ?></a></p>
                        <?php
                            endforeach;
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="panel panel-default panel-curso">
                        <div class="panel-heading">
                            <h3>Turnos</h3>
                        </div>
                        <div class="panel-body">
                        <?php
                            $turnos = get_the_terms(get_the_ID(), 'turno');
                            foreach ($turnos as $key => $value) :
                        ?>
                                <p><?php echo $value->name; ?></p>
                        <?php
                            endforeach;
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="panel panel-default panel-curso">
                        <div class="panel-heading">
                            <h3>Modalidade de ensino</h3>
                        </div>
                        <div class="panel-body">
                        <?php
                            $modalidades = get_the_terms(get_the_ID(), 'modalidade');
                            foreach ($modalidades as $key => $value) :
                        ?>
                                <p><a href="<?php echo get_term_link($value); ?>"><?php echo $value->name; ?></a></p>
                        <?php
                            endforeach;
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    <?php endwhile;?>

    <aside class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </aside>
</div>

<?php get_footer(); ?>
