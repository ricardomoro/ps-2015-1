<?php get_header(); ?>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="row">
            <div class="col-xs-12">
                <h2>Todos os V&iacute;deos</h2>
            </div>
        </div>
        <div class="row">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="embed-responsive embed-responsive-4by3">
                        <?php echo the_content(); ?>
                    </div>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
