<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="row">
    <div class="col-xs-12">
        <article>
            <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <?php if ( has_post_thumbnail()): ?>
                <div class="col-md-7" style="padding-left:0;">
                    <?php the_post_thumbnail( 'medium', array('class' => 'img-responsive') ); ?>
                </div>
            <?php endif; ?>

            <?php echo the_excerpt(); ?>

            <small><?php the_time('d F Y') ?></small>
            
            <a href="<?php the_permalink() ?>" rel="bookmark" class="btn btn-success pull-right">
                Leia mais<span class="sr-only"> sobre &ldquo;<?php the_title(); ?>&rdquo;</span>
            </a>
            <hr />
        </article>
    </div>
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
