<?php if (have_posts()) : ?>
    <!-- Nav tabs -->
    <ul class="nav nav-pills nav-campus" role="tablist">
    <?php $unidade_terms = get_terms( 'unidade' ); ?>
    <?php foreach ($unidade_terms as $key => $unidade) : ?>
        <li <?php echo ($unidade === reset($unidade_terms)) ? 'class="active"' : ''; ?>><a href="#tab<?php echo $unidade->term_id; ?>" data-toggle="tab" role="tab"><?php echo $unidade->name; ?></a></li>
    <?php endforeach; ?>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
    <?php
        foreach ($unidade_terms as $key => $unidade) :
            global $wp_query;
            $args = array(
                'post_parent' => 0,
                'unidade' => $unidade->slug,
            );
            $args = array_merge($wp_query->query_vars, $args);
            query_posts($args);
    ?>
        <div class="tab-pane fade<?php echo ($unidade === reset($unidade_terms)) ? ' in active' : ''; ?>" id="tab<?php echo $unidade->term_id; ?>">
            <?php if (have_posts()) : ?>
                <div class="list-group">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <a href="<?php the_permalink(); ?>" rel="bookmark" class="list-group-item">
                        <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
                        <p class="list-group-item-text"><small>publicado em <?php the_time('d'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?></small></p>
                    </a>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            <?php else : ?>
                <div class="alert alert-warning" role="alert">
                    <p>N&atilde;o existem editais para exibir.</p>
                </div>
            <?php endif; ?>
        </div>
<?php endforeach; ?>
    </div>
<?php else : ?>
    <div class="alert alert-warning" role="alert">
        <p><strong>Aguarde!</strong> Em breve os editais ser&atilde;o publicados.</p>
    </div>
<?php endif; ?>