<?php
function load_styles_2015() {
    // wp_enqueue_style( $handle, $src, $deps, $ver, $media );

    wp_enqueue_style('css-ingresso');
    wp_enqueue_style('css-prettyPhoto');
}

function load_scripts_2015() {
    // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

    wp_enqueue_script('html5shiv');
    wp_enqueue_script('html5shiv-print');
    wp_enqueue_script('respond');
    wp_enqueue_script('respond-matchmedia');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('jquery-prettyPhoto');
    wp_enqueue_script('js-barra-brasil');

    wp_enqueue_script( 'js-prettyPhoto-config', get_stylesheet_directory_uri().'/js/prettyPhoto.config.js', array(), false, true );
}

add_action( 'wp_enqueue_scripts', 'load_styles_2015', 2 );
add_action( 'wp_enqueue_scripts', 'load_scripts_2015', 2 );
