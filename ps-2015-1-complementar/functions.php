<?php
//Registra o menu principal
register_nav_menus(
    array(
        'menu-principal' => 'Menu Principal',
        'menu-lateral' => 'Menu Lateral',
    )
);

// Widgets
require_once('inc/widgets.php');

// Scripts & Styles específicos
require_once('inc/enqueue.php');
