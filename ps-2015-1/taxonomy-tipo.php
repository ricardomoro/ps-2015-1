<?php get_header(); ?>

<section class="row">
    <div class="col-xs-12 col-md-8">
        <h2>Editais do tipo <?php echo single_term_title(); ?></h2>
        <?php echo get_template_part('partials/loop', 'editais'); ?>
    </div>
    <div class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
    </div>
</section>

<?php get_footer(); ?>
