<?php get_header(); ?>

<div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <article class="col-xs-12 col-md-8">
            <div class="row">
                <div class="col-xs-12">
                    <h2><?php the_title(); ?></h2>
                    <p>
                        <strong>Unidade</strong>
                        <?php
                            $unidade = get_the_terms(get_the_ID(), 'unidade');
                            foreach ($unidade as $key => $value) :
                        ?>
                                <span class="label label-info"><?php echo $value->name; ?></span>
                        <?php
                            endforeach;
                        ?>
                    </p>
                    <p>
                        <strong>Tipo</strong>
                        <?php
                            $tipo = get_the_terms(get_the_ID(), 'tipo');
                            foreach ($tipo as $key => $value) :
                        ?>
                                <span class="label label-info"><?php echo $value->name; ?></span>
                        <?php
                            endforeach;
                        ?>
                    </p>
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                <?php
                    $args = array(
                        'post_parent' => get_the_ID(),
                        'post_type' => 'edital',
                        'orderby' => 'menu_order',
                    );
                    $subpages_query = new WP_Query($args);
                ?>
                <?php while ( $subpages_query->have_posts() ) : $subpages_query->the_post(); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="panel-body">
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </article>
    <?php endwhile;?>

    <aside class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
        <?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </div>
            </div>
        <?php endif; ?>
    </aside>
</div>

<?php get_footer(); ?>
