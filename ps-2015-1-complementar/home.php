<?php get_header(); ?>
<?php
    // Get the ID of a given category
    $avisos_id = get_cat_ID( 'avisos' );

    // Get the URL of this category
    $avisos_link = get_category_link( $avisos_id );

    // Get the ID of a given category
    $videos_id = get_cat_ID( 'videos' );

    // Get the URL of this category
    $videos_link = get_category_link( $videos_id );
?>

<div class="row">
    <!-- Banner e Atalhos -->
    <div class="col-xs-12 col-md-4">
        <?php echo get_template_part('partials/atalhos', 'home'); ?>
    </div>
    <!-- Avisos e Vídeo -->
    <div class="col-xs-12 col-md-4">
        <div class="row">
            <div class="col-xs-12">
                <div class="home-titulo">
                    <h2>Avisos</h2>
                    <a href="<?php echo esc_url( $avisos_link ); ?>">Todos os avisos</a>
                </div>
            </div>
        </div>
        <?php
            global $wp_query;
            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field'    => 'slug',
                        'terms'    => 'avisos',
                    ),
                ),
                'posts_per_page' => 4,
            );
            $args = array_merge($wp_query->query_vars, $args);
            query_posts($args);
        ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="aviso-data">
                        <?php the_time('F') ?>
                        <br/>
                        <div class="dia"><?php the_time('d') ?></div>
                    </div>
                    <h3 class="aviso-titulo"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
    <!-- Câmpus -->
    <div class="col-xs-12 col-md-4">
        <div id="lista-campus">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2>C&acirc;mpus do IFRS</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa-ifrs.png" rel="prettyPhoto"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mapa-ifrs.png" alt="Mapa do Rio Grande do Sul com a localização dos câmpus do IFRS."/></a>
                        </div>
                    </div>
                    <div class="row">
                        <ol>
                            <div class="col-xs-6">
                                <li><a href="http://expansao.ifrs.edu.br/">Alvorada</a></li>
                                <li><a href="http://bento.ifrs.edu.br/">Bento Gon&ccedil;alves</a></li>
                                <li><a href="http://canoas.ifrs.edu.br/">Canoas</a></li>
                                <li><a href="http://caxias.ifrs.edu.br/">Caxias do Sul</a></li>
                                <li><a href="http://erechim.ifrs.edu.br/">Erechim</a></li>
                                <li><a href="http://farroupilha.ifrs.edu.br/">Farroupilha</a></li>
                                <li><a href="http://feliz.ifrs.edu.br/">Feliz</a></li>
                                <li><a href="http://ibiruba.ifrs.edu.br/">Ibirub&aacute;</a></li>
                                <li><a href="http://osorio.ifrs.edu.br/">Os&oacute;rio</a></li>
                            </div>
                            <div class="col-xs-6">
                                <li><a href="http://poa.ifrs.edu.br/">Porto Alegre</a></li>
                                <li><a href="http://restinga.ifrs.edu.br/">Restinga</a></li>
                                <li><a href="http://riogrande.ifrs.edu.br/">Rio Grande</a></li>
                                <li><a href="http://expansao.ifrs.edu.br/">Rolante</a></li>
                                <li><a href="http://sertao.ifrs.edu.br/">Sert&atilde;o</a></li>
                                <li><a href="http://expansao.ifrs.edu.br/">Vacaria</a></li>
                                <li><a href="http://expansao.ifrs.edu.br/">Veran&oacute;polis</a></li>
                                <li><a href="http://expansao.ifrs.edu.br/">Viam&atilde;o</a></li>
                            </div>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <hr/>
    </div>
</div>

<?php get_footer(); ?>
